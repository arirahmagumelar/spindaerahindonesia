package dev.trimas.ari.spinnerdaerahindonesia.desa;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import dev.trimas.ari.spinnerdaerahindonesia.R;


public class AdapterSpinnerDesa extends BaseAdapter {
    private LayoutInflater inflator;
    private List<ModelSpinnerDesa> dataList;
    private Context mCtx;

    public AdapterSpinnerDesa(List<ModelSpinnerDesa> dataList, Context mCtx) {
        this.dataList = dataList;
        this.mCtx = mCtx;
        inflator = (LayoutInflater.from(mCtx));
    }

    @Override
    public int getCount() {
        int a ;
        if(dataList != null && !dataList.isEmpty()) {
            a = dataList.size();
        }
        else {
            a = 0;
        }
        return a;
    }

    @Override
    public Object getItem(int i) {
        ModelSpinnerDesa data1 = dataList.get(i);
        return data1.getId_desa();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint({"InflateParams", "ViewHolder"})
    @Override
    public View getView(int i, @Nullable View convertView, @NonNull ViewGroup viewGroup) {
        ModelSpinnerDesa data = dataList.get(i);
        convertView = inflator.inflate(R.layout.spinner_layout, null);
        TextView code_area;
        TextView prov;
        code_area = convertView.findViewById(R.id.code_area);
        prov =  convertView.findViewById(R.id.prov);
        code_area.setText(data.getId_desa());
        prov.setText(data.getDesa());
        return convertView;
    }
}
