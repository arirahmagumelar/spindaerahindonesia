package dev.trimas.ari.spinnerdaerahindonesia;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dev.trimas.ari.spinnerdaerahindonesia.desa.AdapterSpinnerDesa;
import dev.trimas.ari.spinnerdaerahindonesia.desa.ModelSpinnerDesa;
import dev.trimas.ari.spinnerdaerahindonesia.kabupaten.AdapterSpinnerKabupaten;
import dev.trimas.ari.spinnerdaerahindonesia.kabupaten.ModelSpinnerKabupaten;
import dev.trimas.ari.spinnerdaerahindonesia.kecamatan.AdapterSpinnerKecamatan;
import dev.trimas.ari.spinnerdaerahindonesia.kecamatan.ModelSpinnerKecamatan;
import dev.trimas.ari.spinnerdaerahindonesia.provinsi.AdapterSpinnerProvinsi;
import dev.trimas.ari.spinnerdaerahindonesia.provinsi.ModelSpinnerProvinsi;

public class MainActivity extends AppCompatActivity {
    String codeprov,codekab,codekec,codedes,namaprovinsi,namakabupaten,namakecamatan,namadesa;
    Spinner spindesa,spinprov,spinkab,spinkec;
    String tag_json_obj = "json_obj_req";
    EditText plain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
//        }
        getApplicationInfo().flags = ApplicationInfo.FLAG_DEBUGGABLE;
        spinprov = findViewById(R.id.provinsi);
        spinkab = findViewById(R.id.kabupaten);
        spinkec = findViewById(R.id.kecamatan);
        spindesa = findViewById(R.id.desa);
        plain = findViewById(R.id.plain);

            getvalueprovinsi();
            spinprov.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    codeprov = spinprov.getSelectedItem().toString();
                    getvaluekabupaten();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            spinkab.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    codekab = spinkab.getSelectedItem().toString();
                    getvaluekecamatan();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spinkec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    codekec = spinkec.getSelectedItem().toString();
                    getvaluedesa();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spindesa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    codedes = spindesa.getSelectedItem().toString();
                    plain.setText(spinprov.getSelectedItem().toString() +
                            "\n" + spinkab.getSelectedItem().toString() +
                            "\n" + spinkec.getSelectedItem() +
                            "\n" + spindesa.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getvalueprovinsi();
                }
            });
//        } catch (Exception error){
//            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public  void getvalueprovinsi() {
        StringRequest strReq = new StringRequest(Request.Method.POST, Server.PROVINSI, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ModelSpinnerProvinsi modelSpinner = null;
                    JSONObject jObj = new JSONObject(response);
                    String getObject = jObj.getString("results");
                    JSONArray jsonArray = new JSONArray(getObject);
                    ArrayList<ModelSpinnerProvinsi> list = new ArrayList<>();
                    if(jsonArray.length() != 0){
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            modelSpinner = new ModelSpinnerProvinsi();
                            modelSpinner.setId_provinsi(obj.getString("area_code"));
                            modelSpinner.setProvinsi(obj.getString("description"));
                            list.add(modelSpinner);  // TODO Ngambil data yang asli
                        }
                        AdapterSpinnerProvinsi adapter;
                        adapter = new AdapterSpinnerProvinsi(list, MainActivity.this);
                        spinprov.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq,tag_json_obj);
    }

    public  void getvaluekabupaten() {
        StringRequest strReq = new StringRequest(Request.Method.POST, Server.KABUPATEN, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ModelSpinnerKabupaten modelSpinner = null;
                    JSONObject jObj = new JSONObject(response);
                    String getObject = jObj.getString("results");
                    JSONArray jsonArray = new JSONArray(getObject);
                    ArrayList<ModelSpinnerKabupaten> list = new ArrayList<>();
                    if(jsonArray.length() != 0){
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            modelSpinner = new ModelSpinnerKabupaten();
                            modelSpinner.setId_kabupaten(obj.getString("area_code"));
                            modelSpinner.setKabupaten(obj.getString("description"));
                            list.add(modelSpinner);
                        }
                    }
                    AdapterSpinnerKabupaten adapterSpinnerKabupaten;
                    adapterSpinnerKabupaten = new AdapterSpinnerKabupaten(list, MainActivity.this);
                    spinkab.setAdapter(adapterSpinnerKabupaten);
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_provinsi", codeprov);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq,tag_json_obj);
    }

    public  void getvaluekecamatan() {
        StringRequest strReq = new StringRequest(Request.Method.POST, Server.KECAMATAN, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    ModelSpinnerKecamatan modelSpinnerKecamatan;
                    JSONObject jObj = new JSONObject(response);
                    String getObject = jObj.getString("results");
                    JSONArray jsonArray = new JSONArray(getObject);
                    ArrayList<ModelSpinnerKecamatan> list = new ArrayList<>();
                    if(jsonArray.length() != 0){
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            modelSpinnerKecamatan = new ModelSpinnerKecamatan();
                            modelSpinnerKecamatan.setId_kecamatan(obj.getString("area_code"));
                            modelSpinnerKecamatan.setKecamatan(obj.getString("description"));
                            list.add(modelSpinnerKecamatan);
                        }
                        AdapterSpinnerKecamatan adapterSpinnerKecamatan;
                        adapterSpinnerKecamatan = new AdapterSpinnerKecamatan(list, MainActivity.this);
                        spinkec.setAdapter(adapterSpinnerKecamatan);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_kabupaten", codekab);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq,tag_json_obj);
    }

    public  void getvaluedesa() {
        StringRequest strReq = new StringRequest(Request.Method.POST, Server.DESA, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    ModelSpinnerDesa modelSpinnerDesa;
                    JSONObject jObj = new JSONObject(response);
                    String getObject = jObj.getString("results");
                    JSONArray jsonArray = new JSONArray(getObject);
                    ArrayList<ModelSpinnerDesa> list = new ArrayList<>();
                    if(jsonArray.length() != 0){
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            modelSpinnerDesa = new ModelSpinnerDesa();
                            modelSpinnerDesa.setId_desa(obj.getString("area_code"));
                            modelSpinnerDesa.setDesa(obj.getString("description"));
                            list.add(modelSpinnerDesa);
                        }
                        AdapterSpinnerDesa adapterSpinnerDesa;
                        adapterSpinnerDesa = new AdapterSpinnerDesa(list, MainActivity.this);
                        spindesa.setAdapter(adapterSpinnerDesa);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_kecamatan", codekec);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq,tag_json_obj);
    }


}
