package dev.trimas.ari.spinnerdaerahindonesia.kabupaten;

public class ModelSpinnerKabupaten {

    private String id_kabupaten;
    private String kabupaten;

    public ModelSpinnerKabupaten(){

    }
    public ModelSpinnerKabupaten(String id_kabupaten, String kabupaten){
        this.id_kabupaten= id_kabupaten;
        this.kabupaten = kabupaten;
    }

    public String getId_kabupaten() {return id_kabupaten;}

    public void setId_kabupaten(String id_kabupaten) {this.id_kabupaten = id_kabupaten;}

    public String getKabupaten() {return kabupaten;}

    public void setKabupaten(String kabupaten) {this.kabupaten = kabupaten;}

}
