package dev.trimas.ari.spinnerdaerahindonesia.kecamatan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import dev.trimas.ari.spinnerdaerahindonesia.R;

public class AdapterSpinnerKecamatan extends BaseAdapter {

    private LayoutInflater inflator;
    private List<ModelSpinnerKecamatan> dataList;
    private Context mCtx;


    public AdapterSpinnerKecamatan(List<ModelSpinnerKecamatan> dataList, Context mCtx) {
        this.dataList = dataList;
        this.mCtx = mCtx;
        inflator = (LayoutInflater.from(mCtx));
    }

    @Override
    public int getCount() {
        int a ;
        if(dataList != null && !dataList.isEmpty()) {
            a = dataList.size();
        }
        else {
            a = 0;
        }
        return a;
    }

    @Override
    public Object getItem(int i) {
        ModelSpinnerKecamatan data1 = dataList.get(i);
        return data1.getId_kecamatan();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint({"InflateParams", "ViewHolder"})
    @Override
    public View getView(int i, @Nullable View convertView, @NonNull ViewGroup viewGroup) {
        ModelSpinnerKecamatan data = dataList.get(i);
        convertView = inflator.inflate(R.layout.spinner_layout, null);
        TextView code_area;
        TextView prov;
        code_area = convertView.findViewById(R.id.code_area);
        prov =  convertView.findViewById(R.id.prov);
        code_area.setText(data.getId_kecamatan());
        prov.setText(data.getKecamatan());
        return convertView;
    }
}
