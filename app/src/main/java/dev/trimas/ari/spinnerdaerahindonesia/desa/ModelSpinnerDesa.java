package dev.trimas.ari.spinnerdaerahindonesia.desa;

public class ModelSpinnerDesa {
    private String id_desa;
    private String desa;

    public ModelSpinnerDesa(){
    }
    public ModelSpinnerDesa(String id_desa, String desa){
        this.id_desa = id_desa;
        this.desa = desa;
    }

    public String getId_desa() {return id_desa;}

    public void setId_desa(String id_desa) {this.id_desa = id_desa;}

    public String getDesa() {return desa;}

    public void setDesa(String desa) {this.desa = desa;}

}
