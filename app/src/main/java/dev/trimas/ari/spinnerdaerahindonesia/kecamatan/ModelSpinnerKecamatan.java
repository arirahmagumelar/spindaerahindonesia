package dev.trimas.ari.spinnerdaerahindonesia.kecamatan;

public class ModelSpinnerKecamatan {
    private String id_kecamatan;
    private String kecamatan;

    public ModelSpinnerKecamatan(){

    }
    public ModelSpinnerKecamatan(String id_kecamatan, String kecamatan){
        this.id_kecamatan = id_kecamatan;
        this.kecamatan = kecamatan;
    }

    public String getId_kecamatan() {return id_kecamatan;}

    public void setId_kecamatan(String id_kecamatan) {this.id_kecamatan = id_kecamatan;}

    public String getKecamatan() {return kecamatan;}

    public void setKecamatan(String kecamatan) {this.kecamatan = kecamatan;}
}
