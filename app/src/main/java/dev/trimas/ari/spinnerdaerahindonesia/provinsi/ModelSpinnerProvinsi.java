package dev.trimas.ari.spinnerdaerahindonesia.provinsi;

public class ModelSpinnerProvinsi {

    private String id_provinsi;
    private String provinsi;


    public ModelSpinnerProvinsi(){

    }
    public ModelSpinnerProvinsi(String id_provinsi, String provinsi){
        this.id_provinsi = id_provinsi;
        this.provinsi = provinsi;
    }

    public String getId_provinsi() {return id_provinsi;}

    public void setId_provinsi(String id_provinsi) {this.id_provinsi = id_provinsi;}

    public String getProvinsi() {return provinsi;}

    public void setProvinsi(String provinsi) {this.provinsi = provinsi;}

}
